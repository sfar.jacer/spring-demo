package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainRestController {
	
	@GetMapping("/home")
	public String getHomeMessage() {
		return "WELCOME HOME";
	}
}
